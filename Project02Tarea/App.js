import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { text: "" };
    
  }
  onPress = () => {
    this.setState({
      bienvenida:"Bienvenido\nUsuario: "+ this.state.text+"\n" + "contraseña: "+ this.state.contra
      
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={{uri: "https://i.pinimg.com/originals/9f/fc/3c/9ffc3ccd0e89ef2a9e574d5acb1db247.jpg" }}
           style={styles.image}>
          <Text style={styles.titulo}>Bienvenido</Text>
          <Image source={require('./img/logo.png')} style={styles.icono} />
          <Text style={styles.subtitulo}>Ingresar usuario</Text>
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({text})}
            value={this.state.text}
            placeholder="Ingrese su usuario"
            />
          <Text style={styles.subtitulo}>Ingresar contraseña</Text>
          <TextInput
            style={styles.input}
            onChangeText={(contra) => this.setState({contra})}
            value={this.state.contra}
            placeholder="Ingrese su contraseña"
            secureTextEntry={true} password={true}
            />
          <TouchableOpacity style={styles.button} onPress={this.onPress}>
            <Text style={styles.buttonText}>iniciar Sesion</Text>
          </TouchableOpacity>
          <Text style={styles.nombre}>{this.state.bienvenida}</Text>
        </ImageBackground>
        <TouchableOpacity style={styles.button} onPress={() => this.setState({bienvenida:"",text:"",contra:""})}>
          <Text style={styles.buttonText}>Limpiar</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  titulo: {
    color: "white",
    fontSize: 42,
    lineHeight: 84,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000c0"
  },
  subtitulo: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: "center"
  },
  icono: {
    width: 200,
    height: 200,
    margin: 20,
    alignSelf: "center",
    borderRadius: 100
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'white',
    borderWidth: 1,
    marginBottom: 20,
    color: 'black',
    backgroundColor: 'white',
    textAlign: 'center',
    alignSelf: 'center'
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
    margin: 20,
    borderRadius: 10,
  },
  countContainer: {
    alignItems: "center",
    padding: 10
  },
  countText: {
    color: "black",
  },
  nombre: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
    fontFamily: 'monospace'
  }

});

export default App;

